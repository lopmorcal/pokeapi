# Pokemon APP

#### API

Consumiremos la [API pública de Pokemon](https://pokeapi.co/) para listar y consultar el detalle de un Pokemon concreto

#### Stack tecnológico
 - Node + NPM
 - Webpack + Babel
 - HTML/CSS
 - Bootstrap
 - Sass
 - Javascript (ES6)



