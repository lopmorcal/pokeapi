import { paintPokemon } from "./viewlist";

const getDataPokemon = () => {
  fetch("https://pokeapi.co/api/v2/pokemon?limit=151")
    .then((response) => response.json())
    .then((myJson) => {
      const listPokemon = myJson.results;
      console.log(listPokemon);
      paintPokemon(listPokemon);
    })
    .catch((err) => {
      throw new Error(err);
    });
};

const getDataPokemonDetail = async (pokeName) => {
  try {
    const url = `https://pokeapi.co/api/v2/pokemon/${pokeName}`;
    const promise = await fetch(url);
    const response = await promise.json();
    let urlFind = response;

    return urlFind;
  } catch (error) {
    console.error(error, error);
  }
};

export { getDataPokemon, getDataPokemonDetail };
