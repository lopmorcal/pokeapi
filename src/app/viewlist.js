
const main = document.querySelector("main");
const paintPokemon = (objeto) => {
  main.innerHTML = "";
  const ul = document.createElement("ul");
  ul.className = "contpokelist";
  ul.id = "Ulist";
  main.appendChild(ul);
  objeto.forEach((element) => {
    const li = document.createElement("li");
    li.classList.add("contpokelist__card");
    ul.appendChild(li);
    li.innerHTML += `${element.name}`;
  });
};

export { paintPokemon,};
