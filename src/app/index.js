import "./styles/styles.scss";
import { getDataPokemon } from "./GetDataApi";
import { findPokemon } from "./details";

const addListeners = () => {
  document
    .getElementById("listPokemon")
    .addEventListener("click", getDataPokemon);
  document.getElementById("finder").addEventListener("click", findPokemon);
};

window.onload = () => {
  addListeners();
};
