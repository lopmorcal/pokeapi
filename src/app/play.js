const play = () => {
  document.querySelector("footer").innerHTML = `<audio autoplay id="audio" src='/src/app/assets/battle.mp3'></audio>`;
  const main = document.querySelector("main");
  const img = document.getElementById("divImage");
  img.id = "divImage";
  img.className = "class-pokemon";
  main.innerHTML = "";
  const divInsideMain = document.createElement("div");
  divInsideMain.id = "divInsideMainId";
  divInsideMain.className = "divInsideMainClass";
  main.appendChild(divInsideMain);
  const battlefield = document.createElement("div");
  battlefield.id = "battlefieldId";
  battlefield.className = "battlefieldClass";
  divInsideMain.appendChild(battlefield);
  const pointsImg = document.createElement("div");
  pointsImg.id = "signPointsId";
  pointsImg.className = "signPointsClass";
  divInsideMain.appendChild(pointsImg);
  const panelPoints = document.createElement("p");
  panelPoints.id = "panelPointsId";
  panelPoints.className = "panelPointsClass";
  const panelTime = document.createElement("p");
  panelTime.id = "panelTimeId";
  panelTime.className = "panelTimeClass";
  pointsImg.appendChild(panelTime);
  pointsImg.appendChild(panelPoints);
  battlefield.appendChild(img);
  document.getElementById("divImage").addEventListener("click", sumPoints);

  const restTime = () => {
    document.getElementById("panelTimeId").innerHTML = `Tiempo: ${time}`;
    
    time--;
    if (time <= -1) {
      document.getElementById(
        "panelTimeId"
      ).innerHTML = `El Pokemon se escapo!`;

      document.getElementById("panelPointsId").innerHTML = "";

      let count2 = 10;
      const endtime2 = () => {
        document.getElementById(
          "panelPointsId"
        ).innerHTML = `Volviendo al menu principal...`; 
        count2--;
        if (count2 == 0) {
          location.reload();
        }
      };
      setInterval(endtime2, 1000);
    }
  };
  clear = setInterval(restTime, 1000);
};

let points = 0;
let time = 25;
let need = 30;

let clear;
const sumPoints = () => {
  points++;
  const getPoints = document.getElementById("panelPointsId");
  getPoints.innerHTML = `Puntos ${points}/${need}`;
  let randNum = Math.round(Math.random() * 500);
  let randNum2 = Math.round(Math.random() * 500);

  const randMarginTop = document.getElementById("divImage");
  randMarginTop.style.marginTop = randNum + "px";
  const randMarginLetf = document.getElementById("divImage");
  randMarginLetf.style.marginLeft = randNum2 + "px";

  if (points == need) {
    document.getElementById("panelTimeId").innerHTML = `Capturaste al Pokemon!`;

    document.getElementById("panelPointsId").innerHTML = "";

    let count = 10;
    const endtime = () => {
      document.getElementById(
        "panelPointsId"
      ).innerHTML = `Volviendo al menu principal en...${count}`;
      count--;
      if (count == 0) {
        location.reload();
      }
    };
    setInterval(endtime, 1000);

    clearInterval(clear);
  }
};

export { play };
