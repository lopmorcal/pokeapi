import { getDataPokemonDetail } from "./GetDataApi";
import { play } from "./play";

const findPokemon = () => {
  const pokeName = document.querySelector("#input").value.toLowerCase();
  console.log(pokeName);
  if (pokeName == 0) {
    alert("Introduce el nombre CORRECTO de un pokemon");
    location.reload();
  }

  getDataPokemonDetail(pokeName).then((urlFind) => {
    console.log(urlFind);
    let imagePkm;
    let namePkm;
    let idPkm;

    
      imagePkm = urlFind.sprites.front_default;
      namePkm = urlFind.name;
      idPkm = urlFind.id;
    

    const main = document.querySelector(".main");
    main.textContent = "";
    let randomNum = Math.round(Math.random() * 100);
    const divDetails = document.createElement("div");
    divDetails.id = "pokedx";
    divDetails.className = "pokedex";
    divDetails.innerHTML = `<img src="${imagePkm}" id="divImage" class="image">
               <div class="infoPokemon">
               <p>Un ${namePkm} LVL ${randomNum} salvaje aparecio ¡Atrapalo!<br> Num: ${idPkm}</p>
              </div>`;
    const divBall = document.createElement("div");
    divBall.id = "pokePlay";
    divBall.className = "ball";
    divDetails.appendChild(divBall);
    divBall.addEventListener("click", play);

    main.appendChild(divDetails);
  });
};

export { findPokemon };
